#include <GL/glut.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <limits>
#include <sstream>
#include <math.h>
#include <vector>
#include "png++/png.hpp"
#include "spaceEvader.h"

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(200, 200);
    glutCreateWindow("Space Evader");
    Initialize();
    glutKeyboardFunc(Keyboard);
    glutMouseFunc(Mouse);
    glutDisplayFunc(Draw);
    Timer(0);
    glutMainLoop();
    return 0;
}



