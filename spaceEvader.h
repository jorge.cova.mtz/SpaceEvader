//
// Created by jorge_covarrubias on 4/12/17.
//

#ifndef SPACEEVADER_SPACEEVADER_H
#define SPACEEVADER_SPACEEVADER_H

#include "elements.h"
using namespace std;

void Initialize(void);
void Mouse(int,int,int,int);
void Keyboard(unsigned char,int,int);
void Timer(int);
void Draw(void);
GLuint LoadBMP(const char*);


void Initialize() {
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0, 600, 0, 600);
    glMatrixMode(GL_MODELVIEW);
    playerInit();
    textures.push_back(LoadBMP("space.bmp"));
    //textures.push_back(LoadBMP("ship.bmp"));
    textures.push_back(LoadBMP("bluespaceship.bmp"));


    //textures.push_back(LoadBMP("spaceship4.bmp"));
    //gluOrtho2D(0, 500, 0, 500);
    //glClearColor(0.5, 0.5, 0.5, 1);
    //textures.push_back(LoadBMP("floor.bmp"));
    //textures.push_back(LoadBMP("roof.bmp"));
    //textures.push_back(LoadBMP("wall.bmp"));
}

void Mouse(int button, int status, int x,int y){
    std::cout<<"X: "<<x<<std::endl;
    std::cout<<"Y: "<<y<<std::endl;
    if(button == GLUT_LEFT_BUTTON && status == GLUT_DOWN){

    }
}

void Keyboard(unsigned char key, int x, int y){
    switch(key){
        case 'W':
        case 'w':
            break;
        case 'S':
        case 's':
            break;
        case 'D':
        case 'd':
            if(player.Vx2<500) {
                player.Vx1 += velocity;
                player.Vx2 += velocity;
                player.Vx3 += velocity;
                player.Vx4 += velocity;
            }
            break;
        case 'A':
        case 'a':
            if(player.Vx1>0) {
                player.Vx1 += -velocity;
                player.Vx2 += -velocity;
                player.Vx3 += -velocity;
                player.Vx4 += -velocity;
            }
            break;
        default:
            break;
    }
    glutPostRedisplay();
}

void Timer(int iUnused)
{
    glutPostRedisplay();
    glutPostRedisplay();
    if(s==59){
        m++;
        s=0;
        if(m==59){
            h++;
            m=0;
            if(h==12){
                h=0;
                m=0;
                s=0;
            }

        }
    }
    else{
        s++;
        cout<<s<<endl;
    }

    glutTimerFunc(1000, Timer, 0);
}

void Draw(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glColor3f(1.0 , 1.0, 1.0);
    DrawTittles();
    DrawBackground();
    DrawPlayer();
    glutSwapBuffers();
}

GLuint LoadBMP(const char *fileName)
{
    FILE *file;
    unsigned char header[54];
    unsigned int dataPos;
    unsigned int size;
    unsigned int width, height;
    unsigned char *data;
    file = fopen(fileName, "rb");
    if (file == NULL)
    {
        //MessageBox(NULL, L"Error: Invaild file path!", L"Error", MB_OK);
        return false;
    }

    if (fread(header, 1, 54, file) != 54)
    {
        //MessageBox(NULL, L"Error: Invaild file!", L"Error", MB_OK);
        return false;
    }

    if (header[0] != 'B' || header[1] != 'M')
    {
        //MessageBox(NULL, L"Error: Invaild file!", L"Error", MB_OK);
        return false;
    }

    dataPos     = *(int*)&(header[0x0A]);
    size        = *(int*)&(header[0x22]);
    width       = *(int*)&(header[0x12]);
    height      = *(int*)&(header[0x16]);

    if (size == NULL)
        size = width * height * 3;
    if (dataPos == NULL)
        dataPos = 54;

    data = new unsigned char[size];

    fread(data, 1, size, file);

    fclose(file);
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0,3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    return texture;
}


#endif //SPACEEVADER_SPACEEVADER_H
