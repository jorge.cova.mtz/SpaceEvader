//
// Created by jorge_covarrubias on 4/12/17.
//

#ifndef SPACEEVADER_ELEMENTS_H
#define SPACEEVADER_ELEMENTS_H


int velocity = 5;
std::vector<GLint> textures;
int x=400,y=580;
int h=0,m=0,s=0;
int len = 0;
std::stringstream cad;
std::string nombre = "Space Evader";

struct QuadFigure{
    int Vx1;
    int Vy1;
    int Vx2;
    int Vy2;
    int Vx3;
    int Vy3;
    int Vx4;
    int Vy4;
};

struct TriangleFigure{
    int Vx1;
    int Vy1;
    int Vx2;
    int Vy2;
    int Vx3;
    int Vy3;
};
void playerInit();
void DrawBackground();

QuadFigure player;
QuadFigure background;

void playerInit(){
    //Background init
    background.Vx1 = 0;
    background.Vy1 = 570;
    background.Vx2 = 600;
    background.Vy2 = 570;
    background.Vx3 = 600;
    background.Vy3 = 0;
    background.Vx4 = 0;
    background.Vy4 = 0;

    //Player Init
    player.Vx1 = 230;
    player.Vy1 = 60;
    player.Vx2 = 290;
    player.Vy2 = 60;
    player.Vx3 = 290;
    player.Vy3 = 0;
    player.Vx4 = 230;
    player.Vy4 = 0;
}


void DrawBackground(){
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glBegin(GL_QUADS);
    glTexCoord2i(0, 1);glVertex2i(background.Vx1,background.Vy1);
    glTexCoord2i(1, 1);glVertex2i(background.Vx2,background.Vy2);
    glTexCoord2i(1, 0);glVertex2i(background.Vx3,background.Vy3);
    glTexCoord2i(0, 0);glVertex2i(background.Vx4,background.Vy4);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

void DrawPlayer(){
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glBegin(GL_QUADS);
    glTexCoord2i(0, 1);glVertex2i(player.Vx1,player.Vy1);
    glTexCoord2i(1, 1);glVertex2i(player.Vx2,player.Vy2);
    glTexCoord2i(1, 0);glVertex2i(player.Vx3,player.Vy3);
    glTexCoord2i(0, 0);glVertex2i(player.Vx4,player.Vy4);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}


void DrawTittles(){
    len = nombre.length();
    glRasterPos2d(0,y);
    for(int i=0;i<len;i++){
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,nombre[i]);
    }
    cad<<"Tiempo juego: "<<m<<":"<<s;
    std::string cade = cad.str();
    len = cade.length();
    glRasterPos2d(x,y);
    for(int i=0;i<len;i++){
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,cade[i]);
    }
    cad.str("");
}

#endif //SPACEEVADER_ELEMENTS_H
